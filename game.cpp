# include "game.h"
 
/////////// POCETNE POSTAVKE IGRICE !!! ///////////////
void game::set()
{ 
  i=0;
  j=0;
  maxX=440;
  minX=200;
  maxY=450;
  minY=50;
  x=6;
  y=0;
   
  end=false;
  move=true;
  destroy=false;
  rotate=true;
  gameover=false;
  pause=false;
  ////////// HIGH SCORE SET /////////////////
  if (sc.check())
  {
  	  sc.read();
      if (score > sc.getscore())
      {
        sc.setscore(score);
        sc.removef();
        sc.write();	
      }
  }
  else 
    sc.setscore(0);
    sc.write();	
  /////////////////////////////////////////////
  score=0;
  ///////// SET PIECE ////////////////////////
  for(int k=0;k<7;k++)
      for(int l=0;l<4;l++)
        p.setpiece(k,l);
  	
}
void game::sets()
{
	score=0;
}


////////////// SETING STORED PIECES ////////
void game::setstored()
{
int b=minY;	
for(int k=0;k<20;k++)
    for(int l=0;l<12;l++)
		s[l][k].setS(l,k,false);
		score = 0;
}
///////////////// PAUSE ///////////////////
void game::setpt()
{
pause=true;	
}

void game::setpf()
{
pause=false;	
}
////////RETURN END/////////////////////////
bool game::retend()
{
	return end;
}
/////////////////////// KONZOLA //////////////////
int game::consoleX(int b) ///// KONZOLA
{
return cx[b];	
}
int game::consoleY(int b) ///// KONZOLA
{
return cy[b];	
}		
int game::consoleR() ///// KONZOLA
{
return br;	
}
//////////// TEXT /////////////////////////////////////////
void game::txt1(HDC Memhdc)
{
    TCHAR buff[10];
	HFONT hf;
			hf = CreateFont(-14, 0, 0, 10, FW_BOLD, TRUE,
				FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS,
				CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH,
				"Arial");
			SelectObject(Memhdc, hf);
			SetTextColor(Memhdc, RGB(0,255,0));
			SetBkColor(Memhdc, RGB(50,50,50));
	TextOut(Memhdc,450,20,"SCORE:",6);		
	TextOut(Memhdc, 505, 20, buff, wsprintf(buff, TEXT("%d"), score));
	TextOut(Memhdc,450,60,"HIGH SCORE:",11);		
	TextOut(Memhdc, 542, 60, buff, wsprintf(buff, TEXT("%d"), sc.getscore()));	
	DeleteObject(hf);
	DeleteObject(buff);
}	

void game::txt2(HDC Memhdc)
{
        HFONT hf;
	    hf = CreateFont(-8, 0, 0, 10, FW_BOLD, TRUE,
				FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS,
				CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH,
				"Arial");
		SelectObject(Memhdc, hf);
		SetTextColor(Memhdc, RGB(0,255,0));
		SetBkColor(Memhdc, RGB(50,50,50));
		TextOut(Memhdc,20,20,"PRESS ENTER TO RESTART",22);
		TextOut(Memhdc,20,40,"PRESS ESC TO QUIT",17);
		TextOut(Memhdc,20,60,"PRESS SPACE TO ROTATE",21);
		TextOut(Memhdc,20,80,"PRESS LEFT AND RIGHT TO MOVE",28);
		TextOut(Memhdc,20,100,"PRESS DOWN TO SPEED UF FALL",28);
		TextOut(Memhdc,20,120,"PRESS SHIFT TO PAUSE",20);
		TextOut(Memhdc,10,440,"TETRIS++ BY: IVAN GRGA (2016)",30);	
		DeleteObject(hf);
		
}
	
/////////OVDE SE SVE ISCRTAVA!!!!//////////////////////////
void game::draw(HWND hwnd) 
{
	//////////////////////////////DUBLE BUFERING ////PART 1///////////////////	
    HDC Memhdc;
	HDC hdc;
	HBITMAP Membitmap;
	hdc = GetDC(hwnd);
	Memhdc = CreateCompatibleDC(hdc);
	Membitmap = CreateCompatibleBitmap(hdc, 600, 460);
	SelectObject(Memhdc, Membitmap);
	/////////////////////////////////////////////////////////////////////
    SelectObject(Memhdc, GetStockObject(DC_BRUSH));	
    SetDCBrushColor(Memhdc, RGB(50,50,50));	
    Rectangle(Memhdc,0,0,600,460);
        SetDCBrushColor(Memhdc, RGB(0,0,0));
		Rectangle(Memhdc,minX,20,maxX,maxY);
		// TEXT ///////////////////////////////
	txt1(Memhdc);
	/////////////////// OVDE SE SALJE U KONZOLU ////////
	
	/*	for (int m=0;m<4;m++) 
	{
	cx[m]=x+p.getpiecex(i,j,m);
	cy[m]=y+p.getpiecey(i,j,m);
	}*/
    //////////////////////////////////////////////////////////////////////////	
	
	if (gameover)
	TextOut(Memhdc,450,100,"GAME OVER",9);	
	if (pause && !gameover)
	TextOut(Memhdc,450,100,"PAUSE",5);	
		/////////////////////////////////////
		
		/////////// INFO //////////////////////////////////////////////////////////
		txt2(Memhdc);
		//////////////////////////////////////////////////////////////////////////
		p.setpiece(i,j);
		for (int k=0;k<4;k++)
		{   
		    if (k==0)
			SetDCBrushColor(Memhdc, RGB(0,0,255));
			else
			SetDCBrushColor(Memhdc, RGB(0,255,0));
			Rectangle (Memhdc,minX+(x+p.getpiecex(i,j,k))*20,minY+(y+p.getpiecey(i,j,k))*20,minX+(x+p.getpiecex(i,j,k))*20+20,minY+(y+p.getpiecey(i,j,k))*20+20);
		}
	SetDCBrushColor(Memhdc, RGB(255,0,0));	
	for(int k=0;k<20;k++)
   {
    for(int l=0;l<12;l++)
	{
		
		bool full=s[l][k].getSf();
		if (full==true)
		Rectangle (Memhdc,minX+s[l][k].getSx()*20,minY+s[l][k].getSy()*20,minX+s[l][k].getSx()*20+20,minY+s[l][k].getSy()*20+20);
		}
	}	
///////////////////// DB PART 2 ///////////////////////////////////////////////
    BitBlt(hdc, 0, 0, 600, 460, Memhdc, 0, 0, SRCCOPY);
	DeleteObject(Membitmap);
	DeleteDC    (Memhdc);
    ReleaseDC(hwnd, hdc);
//////////////////////////////////////////////////////////////////////////////			
}

void game::input(WPARAM wParam)
{
 key = wParam;
}


/////////OVDE JE LOGIKA IGRICE!!!!//////////////////////////
void game::logic()
{
///////////// GAME OVER /////////////////////////////////
if (!gameover)	
{
   for(int l=0;l<12;l++)
   {
   int k=0;
   if(s[l][k].getSf()==true)
   gameover=true;
   }
////////////////// END SET ////////////////////////////////
end=false;   	
/////////////////////KEY!!!!/////////////////////////////
if (key == VK_SPACE)
{
	j=j+1;
	if (j > 3)
	j=0;
	br=j;
	for (int m=0;m<4;m++) /// konz.
	{
	cx[m]=p.getpiecex(i,j,m);
	cy[m]=p.getpiecey(i,j,m);
	}
		
	for(int k=0;k<20;k++)
      for(int l=0;l<12;l++)
	    for (int m=0;m<4;m++)	
	       if (s[l][k].getSy()==y+p.getpiecey(i,j,m) && s[l][k].getSx()==x+p.getpiecex(i,j,m) && s[l][k].getSf()==true)
	           rotate=false;
	           
    for (int m=0;m<4;m++)
      if ( (x+p.getpiecex(i,j,m))>11 || (x+p.getpiecex(i,j,m))<0 || (y+p.getpiecey(i,j,m))>19)			
        rotate=false; 
		
	if (!rotate)
	{
	  j=j-1;
	  if (j < 0)
	  j=3;	
  	  rotate = true;
	}
			
}	
if (key == VK_LEFT) 
{
	///////////// ZA KONZOLU ////////////////////
	/*	for (int m=0;m<4;m++) /// konz.
	{
	cx[m]=x+p.getpiecex(i,j,m);
	cy[m]=y+p.getpiecey(i,j,m);
	}*/
	//////////////////////////////////////////////
  for(int k=0;k<20;k++)
    for(int l=0;l<12;l++)
	  for (int m=0;m<4;m++)
	    if(s[l][k].getSy()==y+p.getpiecey(i,j,m) && s[l][k].getSx()==x+p.getpiecex(i,j,m)-1)
	      if (s[l][k].getSf()==true)
	        move=false;


    for (int m=0;m<4;m++)
    if (x+p.getpiecex(i,j,m)<=0)
    move=false;
    if (move)
    x--;
    else move =true;
}
	

if (key == VK_RIGHT) 
{	
  for(int k=0;k<20;k++)
    for(int l=0;l<12;l++)
	  for (int m=0;m<4;m++)
	    if(s[l][k].getSy()==y+p.getpiecey(i,j,m) && s[l][k].getSx()==x+p.getpiecex(i,j,m)+1)
	      if (s[l][k].getSf()==true)
	        move=false;


    for (int m=0;m<4;m++)
    if (x+p.getpiecex(i,j,m)>=11)
    move=false;
    if (move)
    x++;
    else move =true;
}
 
//////////////////////////// REDOVI !!!!!   ////////////////////////////	
	
for(int k=0;k<20;k++)
{
	bool line=true;
    for(int l=0;l<12;l++)
    {
    	if (s[l][k].getSf()==false)
 	        line = false;	        	
	}
	if (line)
	   {
	   	nr=k;
	   	destroy = true;
	   }
	   
} 	    
	
	
 if (destroy)
 {
 	for(int k=nr;k>0;k--)
    for(int l=0;l<12;l++)
 	s[l][k].setSf(s[l][k-1].getSf());
 	destroy=false;
 	score=score+10;
	  }	 
////////////////////////////////////////// END !!! //////////////////////
 for(int k=0;k<20;k++)
   {
    for(int l=0;l<12;l++)
	{
		for (int m=0;m<4;m++)
		{
	
	     if(s[l][k].getSy()==y+p.getpiecey(i,j,m)+1 && s[l][k].getSx()==x+p.getpiecex(i,j,m))
	          if (s[l][k].getSf()==true)
	                  end=true;
	 
		}
	}			
}	

   
	
	
	for (int m=0;m<4;m++)
    if (y+p.getpiecey(i,j,m)>=19)
    end=true;
 ////////////////  POPUNJAVANJE ///////////////////////////////////////////////// 
   if (end==true)    
{
 for(int k=0;k<20;k++)
   {
    for(int l=0;l<12;l++)
	{
		for (int m=0;m<4;m++)
		{
	
	 if(s[l][k].getSx()==x+p.getpiecex(i,j,m) && s[l][k].getSy()==y+p.getpiecey(i,j,m))
		s[l][k].setSf(true);
	 
		}
	}			
}	
y=1;
x=6;	
randp();
}
else 
y++;      //////// OVDE KOMAD PADA  //////// 
}
key=0;	///// OVDE SE IZBACUJE TIPKA IZ MEMORIJE ///////			
} 

void game::randp()
{
srand(time(NULL));
i=(rand() % 7);
j=(rand() % 4);	
}

