#ifndef GAME_H   
#define	GAME_H
#include <iostream>
#include <windows.h>
#include "case.h"
#include "stored.h"
# include "score.h"
#include<ctime>
using namespace std;

class game
{
public:
    casep p;
    stored s[12][20];
    hscore sc;
    WPARAM key;
    bool end;
    bool move;
    bool destroy;
    bool rotate;
    bool gameover;
    bool pause;
    int nr;
    int maxX;
    int minX;
    int maxY;
    int minY;
    int x;
    int y; 
    int i;
    int j;
    int score;
    void set();
    void draw(HWND hwnd);
    void input(WPARAM wParam);
    void logic();
    void randp();
    void setstored();
    void setpt();
    void setpf();
    void sets();
    void txt1(HDC Memhdc);
    void txt2(HDC Memhdc);
    int consoleX(int b);
    int consoleY(int b);
    int consoleR();
    int cx[4];
    int cy[4];
    int br;
    bool retend();
};



#endif	/* GAME_H */
