#ifndef CASE_H
#define	CASE_H
#include "piece.h"
#include <iostream>
using namespace std;
// Postoji 7 vrsta i 4 rotacije.
// Znaci sve skupa 28 slucajeva, zato se svi slucajevi spremaju u matricu 7X4.
class casep
{
	public:
	piece P[7][4];
	//i je broj komada, j je broj rotacije, z je broj part-a.
	int getpiecex(int i,int j,int z);
	int getpiecey(int i,int j,int z);
	// x i y su kordinate 0-og tj glavnog part-a od piece-a
	// po kojemu se ovisno o vrsti slazu ostali part-ovi.
	void setpiece(int i,int j);
	void set();
};
#endif	/* CASE_H */ 
