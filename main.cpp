#include <windows.h>
#include"game.h"
#include <stdio.h>
#include <io.h>
#include <fcntl.h>

HDC hdc;
PAINTSTRUCT ps;
game g;
bool pause=false;
bool speed=false;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	PSTR szCmdLine, int iCmdShow)
{
////////////////////////////// DEBUG KONZOLA ////////////////////////////////////////
/* AllocConsole();

    HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
    int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
    FILE* hf_out = _fdopen(hCrt, "w");
    setvbuf(hf_out, NULL, _IONBF, 1);
    *stdout = *hf_out;

    HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
    hCrt = _open_osfhandle((long) handle_in, _O_TEXT);
    FILE* hf_in = _fdopen(hCrt, "r");
    setvbuf(hf_in, NULL, _IONBF, 128);
    *stdin = *hf_in;

    ios::sync_with_stdio();
    */
////////////////////////////////////////////////////////////////////////////////////
	static TCHAR szAppName[] = TEXT("tetris");
	HWND hwnd;
	MSG msg;
	WNDCLASSEX wc;

	
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = szAppName;
	wc.hIconSm = NULL;

	if (!RegisterClassEx(&wc))
	{
		MessageBox(NULL, TEXT("Program requires Windows NT!"),
			szAppName, MB_ICONERROR);
		return 0;
	}

	hwnd = CreateWindow(szAppName,
		NULL,
		WS_POPUP,
		200,
		150,
		600 ,
		460 ,
		NULL,
		NULL,
		NULL,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);


   
		
		while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	
	}


	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	


	

	
	switch (message)
	{
	
	case WM_CREATE:    /////// OVDE SE POSTAVLJAJU POCETNE STVARI ///////
	g.sets();
	g.set();
	g.setstored(); 
	hdc = GetDC(hwnd); 
	SetTimer(hwnd, 1, 200, NULL);  
	ReleaseDC(hwnd, hdc);  
    return 0;

    case WM_KEYDOWN:   //////// OVDE JE UNOS TIPAKA ///////
	g.input(wParam);
		if (wParam == VK_SHIFT)	/////// PAUSE ///////////
		if (pause==false)
		{
		pause=true;	
		g.setpt();
		}
		else 
		{
		pause=false;
		g.setpf();	
		}
		
		if (wParam == VK_RETURN)////// RESTART ///////
		{
		KillTimer(hwnd,1);	
		g.set();
	    g.setstored(); 
	    hdc = GetDC(hwnd); 
	    SetTimer(hwnd, 1, 200, NULL);  
	    ReleaseDC(hwnd, hdc);  	
		}
		
		if (wParam == VK_ESCAPE)////// QUIT ///////////
		PostQuitMessage(0);
		
		if (wParam == VK_DOWN)////// SPEED ///////////
		{
		KillTimer(hwnd,1);
		SetTimer(hwnd, 1, 1, NULL);
		speed=true;	
		}
		
		
	    return 0;

	case WM_TIMER:   /////// OVDE SE ISCRTAVA EKRAN I IZVRSAVAJU SE LOGICKE STVARI //////////
		/////////////// LOGIKA IGRICE //////////////////////////
	    if(!pause)
		g.logic();
		//////////////// KONZOLA I/O //////////////////////////
	/*	system("cls");
		for (int m=0;m<4;m++)
		{
		cout << "X " << m << " = " << g.consoleX(m) << endl;
		cout << "Y " << m << " = " << g.consoleY(m) << endl;	
		}
		cout << "J = " << g.consoleR() << endl;	*/
		///////////////  ISCRTAVANJE EKRANA ////////////////////			
		hdc = GetDC(hwnd);
		g.draw(hwnd);
		ReleaseDC(hwnd, hdc);
		
		//////////////  UBRZANJE PADANJA //////////////////////
		if (speed)
		if (g.retend())	
		{
		KillTimer(hwnd,1);
		SetTimer(hwnd, 1, 200, NULL);
		speed=false;	
		}	
		return 0;
		
    

		
    case WM_DESTROY:
        PostQuitMessage(0);
		return 0;
	}
    
	return DefWindowProc(hwnd, message, wParam, lParam);
}
