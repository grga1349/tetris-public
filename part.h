#ifndef PART_H   
#define	PART_H
#include <iostream>
using namespace std;
// Svaki peace se sastoji od 4 parta koji imaju svoj x i svoj y.
// Ovo je part.
class part
{
	private:
	int  x;
	int  y;
	public:
	void setx(int x);
	void sety(int y);
	int getx();
	int gety();
};
#endif	/* PART_H */
