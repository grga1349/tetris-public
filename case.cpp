# include "case.h"

int casep::getpiecex(int i,int j,int z)
{

return P[i][j].getpx(z);
	
}

int casep::getpiecey(int i,int j,int z)
{

return P[i][j].getpy(z);
		
}

void casep::setpiece(int i,int j)
{
	// VRSTE!!
	int br;
	int x=0;
	int y=0;
	// slucaj I
  if (i==0 )
  {
	br=0;
	P[0][0].setp(x,y,br);
	br=1;
	P[0][0].setp(x,y-1,br);
	br=2;
	P[0][0].setp(x,y-2,br);
	br=3;
	P[0][0].setp(x,y+1,br);
  }
  // slucaj Kocka 
  if (i==1 )
  {
	br=0;
	P[1][0].setp(x,y,br);
	br=1;
	P[1][0].setp(x+1,y,br);
	br=2;
	P[1][0].setp(x,y+1,br);
	br=3;
	P[1][0].setp(x+1,y+1,br);
  }	
  // slucaj T
  if (i==2 )
  {
	br=0;
	P[2][0].setp(x,y,br);
	br=1;
	P[2][0].setp(x+1,y,br);
	br=2;
	P[2][0].setp(x-1,y,br);
	br=3;
	P[2][0].setp(x,y-1,br);
  }
  // slucaj L	
  if (i==3 )
  {
	br=0;
	P[3][0].setp(x,y,br);
	br=1;
	P[3][0].setp(x,y+1,br);
	br=2;
	P[3][0].setp(x,y-1,br);
	br=3;
	P[3][0].setp(x+1,y-1,br);
  }	
  // slucaj L naopako
  if (i==4 )
  {
	br=0;
	P[4][0].setp(x,y,br);
	br=1;
	P[4][0].setp(x,y+1,br);
	br=2;
	P[4][0].setp(x,y-1,br);
	br=3;
	P[4][0].setp(x-1,y-1,br);
  }	
  // slucaj Z
  if (i==5 )
  {
	br=0;
	P[5][0].setp(x,y,br);
	br=1;
	P[5][0].setp(x+1,y,br);
	br=2;
	P[5][0].setp(x,y-1,br);
	br=3;
	P[5][0].setp(x-1,y-1,br);
  }	
  // slucaj Z naopako 
  if (i==6 )
  {
	br=0;
	P[6][0].setp(x,y,br);
	br=1;
	P[6][0].setp(x-1,y,br);
	br=2;
	P[6][0].setp(x,y-1,br);
	br=3;
	P[6][0].setp(x+1,y-1,br);
  }	
  // ROTACIJE
  // 90stupnjeva
  if (j==1)
  {
  	for (int k=0;k<4;k++)
  	{
  	int x1=P[i][0].getpx(k);
  	int y1=P[i][0].getpy(k);
  	P[i][1].setp(-y1,x1,k);
  	}
  }
  // 180 stupnjeva
  if (j==2)
  {
  	for (int k=0;k<4;k++)
  	{
  	int x1=P[i][0].getpx(k);
  	int y1=P[i][0].getpy(k);
  	P[i][2].setp(-x1,-y1,k);
  	}
  }
  // 270 stupnjeva 
  if (j==3)
  {
  	for (int k=0;k<4;k++)
  	{
  	int x1=P[i][0].getpx(k);
  	int y1=P[i][0].getpy(k);
  	P[i][3].setp(y1,-x1,k);
  	}
  } 
}

